# Direction Control of DC Motor

Direction control of DC Motor using L298N or L293D Motor Driver.

Detailed documentation is available at https://telegra.ph/Interface-Motor-Driver-L298N--L293D---DC-Motor-05-24

Thanks for reading! Hope you have enjoyed it. In case of doubts and more interesting discussions on Arduino, Electronics, IoT and Embedded systems 
Please join https://t.me/arduinodeveloper