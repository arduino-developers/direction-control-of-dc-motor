// This code for DC Motor controlled by L298N or L293D Motor Driver.
// Wiring Details
// ENA : 9 , IN1 : 8 & IN2 : 7
// ENB : 3 , IN3 : 4 & IN4 : 5
// OUT1 & OUT2 : Motor 1
// OUT2 & OUT2 : Motor 2
// This code is only for direction control, Please refer separate code available for speedcontrol.
// This code is only applicable for one motor, If needed please add code for second motor at your end.
// Join https://t.me/Arduinodeveloper for more updates and discussions

// Motor 1 Pins initialization
int ENA = 9;
int IN1 = 8;
int IN2 = 7;

void setup() {
  
  // Configuring Pins as outputs
  
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  
  // Putting motors in off mode in starting
  
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
}

void loop() {
  directionControl();
}

// Function for controling direction of motors

void directionControl() {
  // To run the motors in maximum speed setting PWM value to maximum.
  // Incase if you want the motor to run in a particular speed, set the PWM value accordingly.
  // Please refer separate code of speedcontrol for the same.
  // If you don't want speed control you can use normal digital pins also for this purpose, but just enabling the pins as HIGH for ON and LOW for OFF.
  //
  
  analogWrite(ENA, 255);

  // To run Motor 1 & 2 in clock wise rotation
  
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  delay(2000);
  
  // Turn off Motor 1 & 2
  
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  delay(2000);
  
  // To run Motor 1 & 2 in anti-clock wise rotation
  
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  delay(2000);
  
  // Turn off Motor 1 & 2
  
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  delay(2000);
}
